<?php

/**
 * The file that defines any global functions
 *
 * @link       https://graybirch.solutions
 * @since      1.0.0
 *
 * @package    AMC_actdb_shortcode
 * @subpackage AMC_actdb_shortcode/includes
 * @author     Martin Jensen <marty@graybirch.solutions>
 */
