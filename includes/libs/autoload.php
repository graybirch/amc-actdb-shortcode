<?php

/**
 * The file that loads additional libraries
 *
 * Use the require or require_once directive to load libraries as in the examples below
 *
 *    require 'Carbon/autoload.php';
 *    require_once 'Assetic/AssetManager.php';
 *
 * @link       https://graybirch.solutions
 * @since      1.0.0
 *
 * @package    AMC_actdb_shortcode
 * @subpackage AMC_actdb_shortcode/includes
 * @author     Martin Jensen <marty@graybirch.solutions>
 */
