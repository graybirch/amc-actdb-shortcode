<?php

/**
 * The admin (back end) functions of this plugin.
 *
 * @link       https://graybirch.solutions
 * @since      1.0.0
 *
 * @package    AMC_actdb_shortcode
 * @subpackage AMC_actdb_shortcode/admin
 * @author     Martin Jensen <marty@graybirch.solutions>
 */
